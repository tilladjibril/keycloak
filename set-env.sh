#!/bin/bash
set -e

export KC_HOSTNAME_URL=https://keycloak.hostname.org
export POSTGRES_USER=keycloak
export POSTGRES_DB=keycloak
export POSTGRES_PASSWORD=Password123
export KEYCLOAK_ADMIN_PASSWORD=Password123
export KEYCLOAK_FRONTEND_URL=https://keycloak.hostname.org/auth
export KEYCLOAK_ADMIN=admin
export KEYCLOAK_HOSTNAME=keycloak.hostname.org

